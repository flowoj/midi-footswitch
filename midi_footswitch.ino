/*
  https://wokwi.com/projects/347512199847084627
*/

#define BTN_DWN_1 0b00000001
#define BTN_DWN_2 0b00000010
#define BTN_DWN_3 0b00000100
#define BTN_DWN_4 0b00001000
#define BTN_DWN_NEXT 0b00001100
#define BTN_DWN_PREV 0b00000011
#define BTN_DWN_RESET_TIME 0b00000110

#define DEBOUNCE_DELAY 50

//#define DEBUG

#define CC_SCENE 92
#define CC_MSB 0
#define CC_LSB 32

#include <MIDI.h>

#include <LiquidCrystal.h>

// Initialize LCD
const int rs = 12, en = 11, d4 = 10, d5 = 9, d6 = 8, d7 = 7;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

#ifndef DEBUG
// Initialize MIDI
//MIDI_CREATE_DEFAULT_INSTANCE();
//Arduino Every uses Serial1
//https://github.com/FortySevenEffects/arduino_midi_library/issues/300
MIDI_CREATE_INSTANCE(HardwareSerial, Serial1,  MIDI);
#endif

// **************************
// *        VARIABLES       *
// **************************
const int btns [] = { 2, 3, 4, 5 };
const int btnsCount = sizeof(btns) / sizeof(btns[0]);

byte prevBtnState = 0b00000000;
byte currBtnState = 0b00000000;

unsigned long debounceTime = 0;

byte bank = 0;

// **************************
// *          BANKS         *
// **************************
struct ControlChange {
  byte ccNum;
  byte value;
};

struct PerformanceSelect {
  byte msb;
  byte lsb;
  byte program;
};

typedef enum __commandType {
  is_ControlChange, is_PerformanceSelect,
} CommandType;

struct MidiCommand {
  String title;
  String label;
  CommandType type;
  union {
    ControlChange controlChange;
    PerformanceSelect performanceSelect;
  } value;
};

struct Bank {
  String title;
  MidiCommand commands [4];
};

Bank banks [] = {
  {
    "Szenen",
    {
      {
        "Szene 1",
        "sz1",
        is_ControlChange,
        .value = {.controlChange = { CC_SCENE, 0x00 }}
      }, {
        "Szene 2",
        "sz2",
        is_ControlChange,
        .value = {.controlChange = { CC_SCENE, 0x10 }}
      }, {
        "Szene 3",
        "sz3",
        is_ControlChange,
        .value = {.controlChange = { CC_SCENE, 0x20 }}
      }, {
        "Szene 4",
        "sz4",
        is_ControlChange,
        .value = {.controlChange = { CC_SCENE, 0x30 }}
      }
    }
  },
  {
    "Spelunke",
    {
      {
        "Accordeon",
        "acc",
        is_PerformanceSelect,
        .value = {.performanceSelect = { 64, 32, 15 }}
      }, {
        "Accordeon + Strings",
        "a+s",
        is_PerformanceSelect,
        .value = {.performanceSelect = { 64, 32, 14 }}
      }, {
        "Solo Geige",
        "sol",
        is_PerformanceSelect,
        .value = {.performanceSelect = { 64, 32, 16 }}
      }, {
        "Brutal Choir +7",
        "bch",
        is_PerformanceSelect,
        .value = {.performanceSelect = { 64, 32, 8 }}
      }
    }
  },
  // {
  //   "Svarta",
  //   {
  //     {
  //       "Spelunke",
  //       "spe",
  //       is_PerformanceSelect,
  //       .value = {.performanceSelect = { 64, 32, 005 }}
  //     }, {
  //       "Brutal Choir",
  //       "str",
  //       is_PerformanceSelect,
  //       .value = {.performanceSelect = { 64, 32, 8 }}
  //     }, {
  //       "Fuck Off Cunt",
  //       "org",
  //       is_PerformanceSelect,
  //       .value = {.performanceSelect = { 64, 32, 6 }}
  //     }, {
  //       "Troete",
  //       "tro",
  //       is_PerformanceSelect,
  //       .value = {.performanceSelect = { 64, 32, 7 }}
  //     }
  //   }
  // },
  {
    "Mighty D",
    {
      {
        "Koeniglich",
        "koe",
        is_PerformanceSelect,
        .value = {.performanceSelect = { 64, 32, 1 }}
      }, {
        "Drachen",
        "dra",
        is_PerformanceSelect,
        .value = {.performanceSelect = { 64, 32, 1 }}
      }, {
        "Passaunen",
        "pos",
        is_PerformanceSelect,
        .value = {.performanceSelect = { 64, 32, 3 }}
      }, {
        "Orgeln (Wes - Zorg)",
        "wes",
        is_PerformanceSelect,
        .value = {.performanceSelect = { 64, 32, 2 }}
      }
    }
  },
};

byte banksCount = sizeof(banks) / sizeof(banks[0]);


int getSelection() {
  switch (currBtnState) {
    case BTN_DWN_1:
      return 0;
    case BTN_DWN_2:
      return 1;
    case BTN_DWN_3:
      return 2;
    case BTN_DWN_4:
      return 3;
  }
}

void handleBank() {
  MidiCommand command = banks[bank].commands[getSelection()];
  printCurrentSelection(command.title);
  switch (command.type) {
    case is_ControlChange:
      sendCC(command.value.controlChange.ccNum, command.value.controlChange.value);
      break;
    case is_PerformanceSelect:
      selectPerformance(command.value.performanceSelect.msb, command.value.performanceSelect.lsb, command.value.performanceSelect.program);
      break;
  }
}
// **************************
// *           LCD          *
// **************************
void printBank() {
  Bank currentBank = banks[bank];
  int len = currentBank.title.length();
  lcd.home();
  lcd.print("                    ");
  lcd.setCursor((14 - len) / 2, 0);
  lcd.print("<  ");
  lcd.print(currentBank.title);
  lcd.print("  >");
  for (byte i = 0; i < btnsCount; i++ ) {
    // Buttonbeschriftungen
    lcd.setCursor(1 + i * 5, 3);
    lcd.print(currentBank.commands[i].label);
  }
}

void printCurrentSelection(String currentSelection) {
  lcd.setCursor(0, 1);
  lcd.print("                    ");
  lcd.setCursor((20 - currentSelection.length()) / 2, 1);
  lcd.print(currentSelection);
}

void printTimer(unsigned long hr, unsigned long min, unsigned long sec) {
  lcd.setCursor(12, 2);
  if (hr < 10)
    lcd.print("0");
  lcd.print(hr);
  lcd.print(":");
  if (min < 10)
    lcd.print("0");
  lcd.print(min);
  lcd.print(":");
  if (sec < 10)
    lcd.print("0");
  lcd.print(sec);
}

#ifdef DEBUG
void printButtonState() {
  lcd.setCursor(0, 2);
  lcd.print("    ");
  lcd.setCursor(0, 2);
  lcd.print(currBtnState, BIN);
}
#endif

void printWelcomeScreen(){

  byte borders [][8] = {
    {
      B00111,
      B01100,
      B11000,
      B10000,
      B10000,
      B10000,
      B10000,
      B10000
    }, {
      B11111,
      B00000,
      B00000,
      B00000,
      B00000,
      B00000,
      B00000,
      B00000
    }, {
      B11100,
      B00110,
      B00011,
      B00001,
      B00001,
      B00001,
      B00001,
      B00001
    }, {
      B00001,
      B00001,
      B00001,
      B00001,
      B00001,
      B00001,
      B00001,
      B00001
    }, {
      B00001,
      B00001,
      B00001,
      B00001,
      B00001,
      B00011,
      B00110,
      B11100
    }, {
      B00000,
      B00000,
      B00000,
      B00000,
      B00000,
      B00000,
      B00000,
      B11111
    }, {
      B10000,
      B10000,
      B10000,
      B10000,
      B10000,
      B11000,
      B01100,
      B00111
    }, {
      B10000,
      B10000,
      B10000,
      B10000,
      B10000,
      B10000,
      B10000,
      B10000
    }
  };
  for (int i = 0; i < 8; i++) {
    lcd.createChar(i, borders[i]);
  }

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.write(byte(0));
  lcd.print("\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x02");
  lcd.setCursor(0, 1);
  lcd.print("\x07       MIDI       \x03");
  lcd.setCursor(0, 2);
  lcd.print("\x07    Footswitch    \x03");
  lcd.setCursor(0, 3);
  lcd.print("\x06\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x04");
  delay(5000);
  #ifdef DEBUG
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.write(byte(0));
  lcd.print("\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x02");
  lcd.setCursor(0, 1);
  lcd.print("\x07    DEBUG MODE    \x03");
  lcd.setCursor(0, 2);
  lcd.print("\x07 !No Midi output! \x03");
  lcd.setCursor(0, 3);
  lcd.print("\x06\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x04");
  delay(15000);
  #endif
}

// **************************
// *          TIMER         *
// **************************
unsigned long baseTime = 0;

void printTime() {
  unsigned long milli = millis() - baseTime;
  unsigned long hr = milli / 3600000;
  milli = milli - 3600000 * hr;
  unsigned long min = milli / 60000;
  milli = milli - 60000 * min;
  unsigned long sec = milli / 1000;

  printTimer(hr, min, sec);
}

// **************************
// *      MIDI MESSAGES     *
// **************************
void selectPerformance(byte msb, byte lsb, byte program) {
  // MSB CC#0 -> LSB CC#32 -> Program
#ifdef DEBUG
  Serial.println("Select Performance");
  Serial.print("MSB: ");
  Serial.println(msb);
  Serial.print("LSB: ");
  Serial.println(lsb);
  Serial.print("Program: ");
  Serial.println(program);
#else
  MIDI.sendControlChange(CC_MSB, msb, 1);
  MIDI.sendControlChange(CC_LSB, lsb, 1);
  MIDI.sendProgramChange(program - 1, 1); // -1 wegen 0-Basiertem Index.
#endif
}

void sendCC(byte ccNum, byte val) {
#ifdef DEBUG
  Serial.println("Send Control Change");
  Serial.print("CC#: ");
  Serial.println(ccNum);
  Serial.print("Val: ");
  Serial.println(val);
#else
  MIDI.sendControlChange(ccNum, val, 1);
#endif
}

// **************************
// *     INPUT HANDLING     *
// **************************
void getCurrentBtnStates() {
  currBtnState = 0b00000000; // Represent Pressed Buttons as Bitmap
  for (int i = 0; i < btnsCount; i++) {
    currBtnState = currBtnState | ((1 ^ digitalRead(btns[i])) << i);
  }
}

void handle() {
  //next bank
  if (currBtnState == BTN_DWN_NEXT) {
    bank = bank + 1;
    if (bank >= banksCount) {
      bank = 0;
    }
    printBank();
  }
  //prev bank
  else if (currBtnState == BTN_DWN_PREV) {
    bank = bank - 1;
    if (bank >= banksCount) {
      bank = banksCount - 1;
    }
    printBank();
  }
  //reset timer
  else if (currBtnState == BTN_DWN_RESET_TIME) {
    printCurrentSelection("Timer Reset");
    baseTime = millis();
  }
  //handle single button
  else if (currBtnState != 0) {
    handleBank();
  }
}

// **************************
// *         RUNNING        *
// **************************
void setup() {
#ifdef DEBUG
  Serial.begin(9600);
#else
  MIDI.begin();
#endif
  // make the button pins an input:
  for (int btn : btns) {
    pinMode(btn, INPUT_PULLUP);
  }
  // set up the LCD's number of columns and rows:
  lcd.begin(20, 4);
  printWelcomeScreen();
  lcd.clear();
  lcd.home();
  baseTime = millis();
  printBank();
}

void loop() {
  printTime();

#ifdef DEBUG
  printButtonState();
#endif
  getCurrentBtnStates();

  if (prevBtnState != currBtnState) {
    debounceTime = millis();
    prevBtnState = currBtnState;
  }

  if (debounceTime > 0 &&
      prevBtnState == currBtnState &&
      (millis() - debounceTime) > DEBOUNCE_DELAY) {
    handle();
    debounceTime = 0;
  }

  //delay(3);  // delay in between reads for stability
}
